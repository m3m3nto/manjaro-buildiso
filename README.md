# Manjaro BuildISO Docker  Image

A docker image with to build manjaro live ISOs. This uses [acrom18/manjaro-base](https://gitlab.com/acrom18/manjaro-base) as a base image.

## Get The Image

```shell
docker pull registry.gitlab.com/acrom18/manjaro-buildiso
```

