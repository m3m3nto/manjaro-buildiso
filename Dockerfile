FROM registry.gitlab.com/acrom18/manjaro-base

MAINTAINER acrom18

# required implicitly
RUN pacman --quiet --noprogressbar -S --noconfirm lsb-release
RUN pacman --quiet --noprogressbar -S --noconfirm base-devel

# manjaro tools
RUN pacman --quiet --noprogressbar -S --noconfirm manjaro-tools

# install ansible
RUN pacman --quiet --noprogressbar -S --noconfirm ansible

# cleanup
# https://unix.stackexchange.com/questions/52277/pacman-option-to-assume-yes-to-every-question#comment703795_52278
RUN yes | pacman --quiet --noprogressbar -Scc
